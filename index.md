---
lang: en-GB
title: Content Checklist for CERN Websites
---

## Meta data

1. [ ] Word counts: note word counts for different content types
<details><summary>Details</summary>
    - Word counts in French are 30% longer
    - Announcements: 200 words (EN) 260 words (FR)
    - News / Official news: 400 words (EN) 520 words (FR)
    - Press release / Opinion / Obituary: 500 words (EN) 650 words (FR)
    - Feature: 1200 words (EN) 1560 words (FR)
</details>

1. [ ] Captions: 255 characters
<details><summary>Details</summary>
    - Please _do not_ exceed this limit. If you need to describe an image in great detail, include the description in the body of the text with a reference to the image in question
    - Also bear in mind that French texts tend to be longer, so try and avoid English captions that are more than 200 characters in length
</details>

1. [ ] Straps: do not end with a full stop

1. [ ] Listing title: aim for 45 characters including spaces
<details><summary>Details</summary>
    - Listing title does _not_ need to be the same as main title
    - Titles longer than 45 characters including spaces will show only the first 45 characters then ... on the homepage
</details>

1. [ ] Listing strap: 150 characters
<details><summary>Details</summary>
    - Listing strap does _not_ need to be the same as main strap -- it can be a teaser, since it is displayed on listings, while the main strap appears in context at the top of the article
</details>

1. [ ] Use audience(s), topic and tag(s) taxonomy <button type="button" class="btn btn-blue">home.cern</button>
<details><summary>Details</summary>
    - Possible audiences are `General public`, `CERN community`, `Educators`, `Students`, `Scientists`, `Industry`, `Media` and `Policy makers`
    - Possible topics are `Physics`, `Accelerators`, `Experiments`, `Engineering`, `Computing`, `Knowledge sharing` and `At CERN`. (Use `At CERN` sparingly and only if no other topic is relevant)
    - There are many tags available, the easist is to see which tags were used for a similar article in the past. If in doubt, [contact the web editor](mailto:web-editor@cern.ch)
</details>

1. [ ] Tag news so it appears elsewhere <button type="button" class="btn btn-blue">home.cern</button>
<details><summary>Details</summary>
    - Use `CERNimpact` for news to appear on the [Contribute to society](https://home.cern/about/what-we-do/our-impact) page 
    - Use `energy` for news to appear on the [Energy management](https://hse.cern/content/energy-management) page
    - Use `environment` for news to appear on the [Environmentally responsible research](https://home.cern/about/what-we-do/environmentally-responsible-research) page
    - Use `Higgs boson` for news to appear on the [Higgs boson](https://home.cern/science/physics/higgs-boson) page
    - Use `Human resources` or `CHIS` on relevant official news to appear on HR and CHIS websites
    - Use `international-relations` for news to appear on [Bring nations together](https://home.cern/about/what-we-do/bring-nations-together) page
</details>

1. [ ] Provide revision information <button type="button" class="btn btn-blue">home.cern</button>
<details><summary>Details</summary>
    - Summarise change or use these suggestions:
`[Fix typo]` • `[Correct error]` • `[Edit meta data]` • `[Update outdated content]` • `[Add new content]`
</details>

## Text content

1. [ ]  Check style and grammar
<details><summary>Details</summary>
    - Use the [CERN English and French Style Guides](https://translation-council-support-group.web.cern.ch/style-guides)
    - No American spellings unless they are part of formal names. 
    - British English spellings except when referring to CERN as "the Organization" -ise not -ize (organise, recognise etc), -re not -er (centre, metre, kilometre etc), -our not -or (behaviour, colour etc)
    - For *Italics*, EN text should use them *sparingly*, for emphasis. They can be used for *journal names* but not for quotes, where "quotation marks" do the job. In FR text, quotes need to be *italic* because quotes don't "close" before you see who said the quote, and then "reopen" afterwards. The lack of italics makes it harder to parse the info. 
</details>

1. [ ]  Check that all links are valid
<details><summary>Details</summary>
    - For all links to content on `home.cern`, remove `http(s)://home.cern` from the URL i.e. change https://home.cern/science/physics/antimatter to /science/physics/antimatter
</details>

1. [ ] Remove extraneous/incorrect formatting tags in the HTML code
<details><summary>Details</summary>
    - Switch to `Basic HTML` first, paste in the text (from LibreOffice or Word, e.g.), and then switch back to `CERN Full HTML`
    - Make sure there are no additional spaces, characters or tags (e.g. `<span>`) inserted in the text
</details>

1. [ ] Use `&nbsp;` instead of regular spaces where needed <button type="button" class="btn btn-green">a11y</button>
<details><summary>Details</summary>
    - Numbers with units (e.g. 500 kg) or dates (e.g. 22 November) should have `&nbsp;` instead of regular spaces. e.g.: `500&nbsp;kg` or `22&nbsp;November`
    - For _French_ text, quotation marks and colons should have `&nbsp;` as well: `«&nbsp;`, `&nbsp;»` and `&nbsp;:`
</details>

1. [ ] Replace dashes for showing range with the words "to" or "and" <button type="button" class="btn btn-green">a11y</button>
<details><summary>Details</summary>
    - Say "6 and 7 June" instead of "6&ndash;7 June" or "6 to 9 June" instead of "6&ndash;9 June" is better for those with screen readers
</details>

1. [ ] Use pretty punctuation
<details><summary>Details</summary>
    - Don't use typewriter quotes, but "pretty" quotes instead: \"text\" vs &ldquo;text&rdquo;
    - The same goes for apostrophes: l\'air vs l&rsquo;air
</details>

1. [ ] Use correct symbols: minus / dash / hyphen and degrees <button type="button" class="btn btn-green">a11y</button>
<details><summary>Details</summary>
    - A minus (&minus;), an en&nbsp;dash (&ndash;) and a hyphen (\-) are rendered differently and are read differently by screen readers. Use the appropriate symbol
        - Particularly important when the minus (`&minus;`) is supposed to be in a superscript: 10<sup>&minus;32</sup>, not 10<sup>-32</sup>
        - Separators, when used, should be rendered as en-dashes (&ndash;) using `&ndash;`, not hyphens
        - Do _not_ use em&nbsp;dashes (&mdash;) in English.
    - Do not use a superscripted `0` (<sup>0</sup>C) or a superscripted `o` (<sup>o</sup>C) when you mean to use the degree symbol (&deg;C): `&deg;`
</details>

1. [ ] Acknowledge if the French version is coming later <button type="button" class="btn btn-blue">home.cern</button>
<details><summary>Details</summary>
    - If the English is published before the French, this is the recommended text to use for the French page: `<p><strong><em>La version française de cet article n’est pas disponible pour le moment. Nous faisons tout notre possible pour la mettre en ligne dans les plus brefs délais. Merci de votre compréhension.</em></strong></p><p><strong>___</strong></p>`
</details>

1. [ ] Use [MathJax](https://www.mathjax.org/) for LaTeX equations, but only if required
<details><summary>Details</summary>
    - MathJax allows you to display equations and scientific notations in HTML
    - You bookend your LaTex code with dollar signs: `$YOUR-LATEX-CODE$`. For example, `$B_s^0$` will convert to “B-subscript-s-superscript-0”. If you are confused about the exact LaTex code to use, please consult your friendly neighbourhood physicist
    - Please use it when required (and only when required); use [unicode](https://home.unicode.org/) as a first option always (see [list of unicode characters](https://en.wikipedia.org/wiki/List_of_Unicode_characters)).
    - However, **do not be tempted** to replace regular unicode/HTML characters with Latex where unnecessary: `$\mu$` will convert to the Greek letter µ with MathJax, but this is already a valid unicode character and should be pasted in as is (or using the HTML code `&mu;` when entering text in the HTML editor in Drupal). See, for example, [Achintya’s article on the LHCb tetraquark](https://home.cern/news/news/physics/lhcb-discovers-first-open-charm-tetraquark/), which uses pure unicode (including arrows and non-breaking spaces).
</details>

## Multimedia content

1. [ ] Check if the image is on CDS first before uploading a local image or a special-category CDS image
<details><summary>Details</summary>
    - Ask those who submit images as part of the article if they are already on CDS
</details>

1. [ ] Optimise images before uploading (local or CDS) <button type="button" class="btn btn-green">a11y</button>
<details><summary>Details</summary>
    - **CDS images**: _Minimum_ width of 1440 px at 72 pixels/inch
    - **Local images**: _Maximum_ width of 1440 px at 72 pixels/inch
    - Reduce the quality by optimising the image using (1) [
        Compres JPEG](https://compressjpeg.com/) (2) [ImageOptim](https://imageoptim.com/online) or (3) [Squoosh](https://squoosh.app/)
    - When uploading to CDS, provide clear descriptions, and assign credit correctly
    - When uploading a local image, _please provide correct alt text_ <button type="button" class="btn btn-green">a11y</button>
    - If the image looks stretched, add `height="auto"` so that `<img src=“….”>` becomes `<img height="auto" src=“….”>`
</details>

1. [ ] Link credits in captions (for inline images and videos) to CDS / `videos.cern.ch`
<details><summary>Details</summary>
    - You may need to modify the `source` of the article to add the link
</details>

1. [ ] Embed CDS slideshow at end of article
<details><summary>Details</summary>
    - Go to the CDS entry, click on `View as Slideshow` and then click on `Embed record as a slideshow` below the images
    - Cross-link back to the CDS entry
</details>

1. [ ] Check image sizes for listing and main images
<details><summary>Details</summary>
    - Listing images from CDS should be `medium` (and in some cases `small`) but never `large`. It depends on how pixelated the image looks. Some look fine with small, others need medium.
    - Main images should _always_ be `large`
</details>

1. [ ]  YouTube videos should include `nocookie` and `?rel=0` in the URL and have subtitles enabled
<details><summary>Details</summary>
    - Change `youtube.com/embed/` to `youtube-nocookie.com/embed/` to avoid youTube cookies and to be "privacy friendly"
    - If subtitles are available, force them to display by appending `&amp;cc_lang_pref=en&amp;cc_load_policy=1` to `?rel=0`, where `cc_lang_pref` should be `en` or `fr` depending on the article's language
</details>

1. [ ]  Add subtitles to videos <button type="button" class="btn btn-green">a11y</button> 
<details><summary>Details</summary>
    - Keep subtitles concise, use commas and question marks but avoid full stops. 
    - Do not use titles such as Dr, Prof etc. 
    - Avoid splitting sentences across subtitles, if possible. 
    - Stay as faithful as possible to the audio, but if a speaker uses too many words or incorrect words, paraphrase using grammatically correct subtitles.
    - [Subtitle Edit](https://www.nikse.dk/subtitleedit) is a useful tool with hints.
</details>
 
1. [ ]  Videos to no longer mention the editor on web pages
<details><summary>Details</summary>
    - Should only say `Video: CERN` (in EN) and `Video : CERN` (in FR).
    - Use the description of the video on https://videos.cern.ch/ and/or on Youtube to credit the people involved.
</details>

1. [ ] [**After publishing**] Contact the audiovisual team to ask them to cross-link to article in CDS
<details><summary>Details</summary>
    - This improves content discovery, when combined with slideshow embeds and linking credits
</details>

---

## Notes, references and further reading

1. [CERN English and French Style Guides](https://translation-council-support-group.web.cern.ch/style-guides)
1. Accessibility / a11y:
    1. [The A11Y Project](https://a11yproject.com)
    1. [Mozilla web docs on Accessibility](https://developer.mozilla.org/en-US/docs/Learn/Accessibility)
    1. [Mozilla web docs on Accessibility in HTML](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/HTML)
    1. [Writing effective alt text - EN](https://design102.blog.gov.uk/2022/01/14/whats-the-alternative-how-to-write-good-alt-text/)
    1. [Writing effective alt text - FR](https://www.twaino.com/definition/a/alt-text/)
1. [W3C Character Entity Reference Chart](https://dev.w3.org/html5/html-author/charref)

